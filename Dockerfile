FROM public.ecr.aws/ubuntu/ubuntu:22.04_stable
LABEL Name="runner-base"
LABEL Version="1.2.1"

ARG TARGETARCH
ENV NODE_MAJOR=18

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      apt-transport-https software-properties-common ca-certificates build-essential \
      ssh curl gnupg lsb-release locales zip unzip git jq vim nano dnsutils \
    && rm -rf /var/lib/apt/lists/*

RUN add-apt-repository -y ppa:deadsnakes/ppa

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      python3.11-dev python3.11-distutils \
    && rm -rf /var/lib/apt/lists/*

RUN curl -OL https://bootstrap.pypa.io/get-pip.py \
    && python3.11 get-pip.py --no-input --no-cache-dir \
    && rm get-pip.py

RUN update-locale LC_ALL=C.UTF-8 LANG=C.UTF-8

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
  | apt-key add -
RUN curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key \
  | apt-key add -
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key \
  | apt-key add -

RUN add-apt-repository "deb [arch=$TARGETARCH] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
RUN add-apt-repository "deb [arch=$TARGETARCH] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /"
RUN add-apt-repository "deb [arch=$TARGETARCH] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main"

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      docker-ce docker-buildx-plugin kubectl nodejs \
    && rm -rf /var/lib/apt/lists/*

RUN if [ "$TARGETARCH" = "arm64" ]; then PLAT="aarch64"; else PLAT="x86_64"; fi \
    && curl -fLo awscliv2.zip https://awscli.amazonaws.com/awscli-exe-linux-$PLAT.zip \
    && unzip -q awscliv2.zip \
    && ./aws/install \
    && rm -rf awscliv2.zip /aws

RUN pip3.11 install --no-cache-dir -q -U poetry poetry-plugin-export pipenv pylint coverage
