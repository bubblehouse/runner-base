## [1.2.1](https://gitlab.com/bubblehouse/runner-base/compare/v1.2.0...v1.2.1) (2024-03-17)


### Bug Fixes

* install more testing and lint dependencies ([685f813](https://gitlab.com/bubblehouse/runner-base/commit/685f8133fb48058e75b5af8d7af6e903e9cc253c))

## [1.2.0](https://gitlab.com/bubblehouse/runner-base/compare/v1.1.1...v1.2.0) (2024-03-17)


### Features

* install python 3.11 ([964260d](https://gitlab.com/bubblehouse/runner-base/commit/964260d0903d9450bdde798c0ed69f91c3ca8cfa))

## [1.1.1](https://gitlab.com/bubblehouse/runner-base/compare/v1.1.0...v1.1.1) (2023-09-17)


### Bug Fixes

* remove cache after installing pip packages ([0b0255c](https://gitlab.com/bubblehouse/runner-base/commit/0b0255c15d87a03174829bbbf5e8634bf6b3857f))

## [1.1.0](https://gitlab.com/bubblehouse/runner-base/compare/v1.0.2...v1.1.0) (2023-09-17)


### Features

* add python packaging tools ([a159eef](https://gitlab.com/bubblehouse/runner-base/commit/a159eefd4cbad969d1e81bd0a668c535e442a174))

## [1.0.2](https://gitlab.com/bubblehouse/runner-base/compare/v1.0.1...v1.0.2) (2023-09-17)


### Bug Fixes

* install pip and python3 ([aadc220](https://gitlab.com/bubblehouse/runner-base/commit/aadc220df61577906da501ccf24fcbe0132d397a))

## [1.0.1](https://gitlab.com/bubblehouse/runner-base/compare/v1.0.0...v1.0.1) (2023-09-17)


### Bug Fixes

* bootstrap to node 18 ([6154b3f](https://gitlab.com/bubblehouse/runner-base/commit/6154b3f4af400038a2966f67ab9e212eb1de670d))
* bootstrap to node 18 ([291d028](https://gitlab.com/bubblehouse/runner-base/commit/291d028a73bfa0dbee399267bdb19bf54b2000be))
* dockerfile fixes, .gitignore, lockfile ([59556ce](https://gitlab.com/bubblehouse/runner-base/commit/59556ce8ce6280a138e3510a2b70c64a1af22d4d))
* remove spurious dir creation ([c0cfa8c](https://gitlab.com/bubblehouse/runner-base/commit/c0cfa8c6fc717c468d21a4bd3f66365ca08cf389))
* rollback node.js to 18 ([a10f19c](https://gitlab.com/bubblehouse/runner-base/commit/a10f19c41163d559a2fae133d3c6c3e5cac4831d))
* update node.js to v20 ([5d88274](https://gitlab.com/bubblehouse/runner-base/commit/5d882742eb766d537168c8b4b2766af5be69669f))

## 1.0.0 (2023-09-17)


### Bug Fixes

* initial commit ([d38aeb8](https://gitlab.com/bubblehouse/runner-base/commit/d38aeb85970f65f388ebbdd8272024f713ce7f97))
